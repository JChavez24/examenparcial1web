const form= document.getElementById("form");
const inputs = document.querySelectorAll('#form input');
const adver = document.getElementById('mensaje');
const expresionesRegulares = {
    numeros : /([0-9])/,
    text : /([a-zA-Z])/,
    caracteres : /^[^a-zA-Z\d\s]+$/,
    correo : /^([a-z\d]+[@]+[a-z]+\.[a-z]{2,})+$/,
    espacios : /\s/g
}

inputs.forEach(input=>{
    input.addEventListener('keyup', (e) => {   
        let valueInput = e.target.value;
        switch(e.target.id){

            case 'codigo':
                
                if(e.target.value.length == 5){
                    e.target.style.border ='3px solid black';
                    adver.innerHTML = '';
                }else{
                    e.target.style.border = '3px solid red';
                    adver.innerHTML = 'Debe ingresar un codigo de 5 caracteres máximo';
                }
            break;
            case 'titulo':
                
                if((e.target.value.length >= 10) && (e.target.value.length <=100)){
                    e.target.style.border ='3px solid black';
                    adver.innerHTML = '';
                }else{
                    e.target.style.border = '3px solid red';
                    adver.innerHTML = 'Debe ingresar un titulo de entre 10 y 100 caracteres.';
                }
            break;
            case 'autor':
                
                if((e.target.value.length >= 10) && (e.target.value.length <=60)){
                    e.target.style.border ='3px solid black';
                    adver.innerHTML = '';
                }else{
                    e.target.style.border = '3px solid red';
                    adver.innerHTML = 'Debe ingresar un autor de entre 10 y 60 caracteres.';
                }
            break;
            case 'editorial':
                
                if((e.target.value.length >= 10) && (e.target.value.length <=30)){
                    e.target.style.border ='3px solid black';
                    adver.innerHTML = '';
                }else{
                    e.target.style.border = '3px solid red';
                    adver.innerHTML = 'Debe ingresar una editorial de entre 10 y 30 caracteres.';
                }
            break;
        }
    })
});

form.addEventListener("submit", e=>{
    e.preventDefault();
    var codigo = document.getElementById('codigo');
    var titulo = document.getElementById('titulo');
    var autor = document.getElementById('autor');
    var editorial = document.getElementById('editorial');
    var fpubli = document.getElementById('fpubli');
    var fingreso = document.getElementById('fingreso');

    if(codigo.value==""){
        adver.innerHTML = "Debe ingresar todos los campos";
    }else if(titulo.value== ""){
        adver.innerHTML = "Debe ingresar todos los campos";
    }else if(autor.value==""){
        adver.innerHTML = "Debe ingresar todos los campos";
    }else if(editorial.value==""){
        adver.innerHTML = "Debe ingresar todos los campos";
    }else if(fpubli.value==""){
        adver.innerHTML = "Debe ingresar todos los campos";
    }else if(fingreso.value==""){
        adver.innerHTML = "Debe ingresar todos los campos";
    }else if (fingreso.value < fpubli.value) {
        alert("La fecha de ingreso debe ser mayor a la fecha de publicación");
        return false;
      }
    else{
        adver.innerHTML = '';
        alert("Datos enviados exitosamente!");
    }
      
})
